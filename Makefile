SKIP_SQUASH?=1
FRONTNAME=opsperator
-include Makefile.cust

.PHONY: build
build:
	@@docker build -t application -f Dockerfile.base .
	@@docker build -t scalelite:api -f Dockerfile.api .
	@@docker build -t scalelite:nginx -f Dockerfile.nginx .
	@@docker build -t scalelite:poller -f Dockerfile.poller .
	@@docker build -t scalelite:recordings -f Dockerfile.recording-importer .

.PHONY: presentation
presentation:
	@@if test -d presentation; then \
	    mv presentation presentation.`date +%s`; \
	fi
	@@docker rm -f slrecs || echo ok
	@@slvers=`grep BUILD_NUMBER Dockerfile.base | awk -F= '{print $$2;exit 0;}'`; \
	docker run --name slrecs \
	    --entrypoint /bin/sh \
	    -d docker.io/blindsidenetwks/scalelite:v$$slvers-nginx \
	    -c 'sleep 86400'
	@@docker cp slrecs:/var/bigbluebutton/playback/presentation presentation
	@@docker rm -f slrecs

.PHONY: test
test:
	@@docker rm -f redis || echo true
	@@docker run --name redis -d docker.io/redis:6
	@@docker rm -f postgres || echo true
	@@docker run --name postgres \
	    -e POSTGRESQL_USER=pguser \
	    -e POSTGRESQL_DATABASE=pgdb \
	    -e POSTGRESQL_PASSWORD=pgpassword \
	    -d docker.io/centos/postgresql-12-centos7:latest
	@@sleep 10
	@@redisctn=`docker ps | awk '/redis/{print $$1;exit 0;}'`; \
	redisip=`docker inspect $$redisctn | awk '/"IPAddress": "/{print $$2;exit 0}' | cut -d'"' -f2`; \
	pgctn=`docker ps | awk '/postgres/{print $$1;exit 0;}'`; \
	pgip=`docker inspect $$pgctn | awk '/"IPAddress": "/{print $$2;exit 0}' | cut -d'"' -f2`; \
	( \
	    echo "REDIS_URL=redis://$$redisip:6379/"; \
	    echo "DATABASE_URL=postgres://pguser:pgpassword@$$pgip:5432/pgdb?pool=5&sslmode=disable"; \
	    echo "LOADBALANCER_SECRET=thisismytestsecretforgitlabci"; \
	    echo "SECRET_KEY_BASE=thisismytestsecretforgitlabci"; \
	    echo "URL_HOST=mybbb.demo.local"; \
	) >.env
	@@mkdir -p ./wip-recordings/spool ./wip-recordings/published
	@@chmod 777 ./wip-recordings ./wip-recordings/spool \
	    ./wip-recordings/published
	@@docker rm -f scalelite-api || echo true
	@@docker run --name scalelite-api \
	    --env-file=./.env \
	    -v `pwd`/wip-recordings:/var/bigbluebutton \
	    -d scalelite:api
	@@sleep 5
	@@docker exec -it scalelite-api /bin/sh -c './bin/rake db:migrate'
	@@docker rm -f scalelite-poller || echo true
	@@docker run --name scalelite-poller \
	    --env-file=./.env \
	    -d scalelite:poller
	@@docker rm -f scalelite-recordings-importer || echo true
	@@docker run --name scalelite-recordings-importer \
	    --env-file=./.env \
	    -v `pwd`/wip-recordings:/var/bigbluebutton \
	    -d scalelite:recordings

.PHONY: kubebuild
kubebuild: kubecheck
	@@for f in image git task pipeline pipelinerun-base; \
	    do \
		kubectl apply -f deploy/kubernetes/tekton-$$f.yaml; \
	    done; \
	echo Make sure to apply deploy/kubernetes/tekton-pipelinerun-deps; \
	echo once the base pipelinerun would have built Scaleilte base image

.PHONY: kubecheck
kubecheck:
	@@kubectl version >/dev/null 2>&1 || exit 42

.PHONY: kubedeploy
kubedeploy: kubecheck
	@@for f in configmap service statefulset secret deployment; \
	    do \
		kubectl apply -f deploy/kubernetes/$$f.yaml; \
	    done
